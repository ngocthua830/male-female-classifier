import joblib
import os
import numpy as np
from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from random import shuffle
import sys

model = VGG16(weights='imagenet', include_top=False)
model.summary()

IMG_SIZE = 300

clf = joblib.load("model_cnn_svm_vgg16.sav")

DIR = "/dataset/Students/thua/male_female/test"

def label_img(Dir):
    if Dir == DIR+"/male": return np.array(0)
    elif Dir == DIR+'/female' : return np.array(1)

def evaluate():
    total_img = 0
    right = 0
    TRAIN_DIR = [DIR+'/male', DIR+'/female']
    for Dir in TRAIN_DIR:
        for img in os.listdir(Dir):
             label = label_img(Dir)
             path = os.path.join(Dir, img)
             img = image.load_img(path, target_size=(224, 224))
             img_data = image.img_to_array(img)
             img_data = np.expand_dims(img_data, axis=0)
             img_data = preprocess_input(img_data)
             vgg16_feature = model.predict(img_data)
             vgg16_feature_np = np.array(vgg16_feature)
             test_data = [vgg16_feature_np.flatten()]
             pred = clf.predict(test_data)
             if int(pred) == int(label):
                 right += 1
             total_img += 1
    print(right, total_img)

evaluate()

def predict_img(path):
    img = image.load_img(path, target_size=(224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = preprocess_input(img_data)
    vgg16_feature = model.predict(img_data)
    vgg16_feature_np = np.array(vgg16_feature)
    test_data = [vgg16_feature_np.flatten()]
    pred = clf.predict(test_data)
    return pred

if len(sys.argv) > 1:
    pred = predict_img(sys.argv[1])
else:
    pred = predict_img("/dataset/Students/thua/male_female/test/male/44903_1921-03-11_1971.jpg")

if int(pred) == 0:
    print('male')
else:
    print('female')

