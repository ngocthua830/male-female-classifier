from PIL import Image
import numpy as np
import os
from random import shuffle
import matplotlib.pyplot as plt

DIR = "/dataset/Students/thua/male_female/train"

def label_img(Dir):
    if Dir == DIR+"/male": return np.array([1, 0])
    elif Dir == DIR+'/female' : return np.array([0, 1])

IMG_SIZE = 300

def load_training_data():
    train_data = []
    TRAIN_DIR = [DIR+"/male", DIR+"/female"]
    for Dir in TRAIN_DIR:
        for img in os.listdir(Dir):
            label = label_img(Dir)
            path = os.path.join(Dir, img)
            if "DS_Store" not in path:
                img = Image.open(path)
                img = img.convert('L')
                img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
                train_data.append([np.array(img), label])
    shuffle(train_data)
    return train_data

train_data = load_training_data()
trainImages = np.array([i[0] for i in train_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
trainLabels = np.array([i[1] for i in train_data])

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
import numpy as np

model = Sequential()
model.add(Conv2D(32, kernel_size = (3,3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, 1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(BatchNormalization())

model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(BatchNormalization())

model.add(Conv2D(96, kernel_size=(3,3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(BatchNormalization())

model.add(Conv2D(96, kernel_size=(3,3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(BatchNormalization())

model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(BatchNormalization())
model.add(Dropout(0.2))

#model.add(Flatten())
#model.add(Dense(256, activation='relu'))
#model.add(Dropout(0.2))
#model.add(Dense(128, activation='relu'))
#model.add(Dense(2, activation='softmax'))
img = Image.open(path)
img = img.convert('L')
img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTI
model.predict(
