from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
import joblib
from random import shuffle
from sklearn import svm
import numpy as np
import os

model = VGG16(weights='imagenet', include_top=False)
model.summary()

DIR = "/dataset/Students/thua/male_female/train"
def label_img(Dir):
    if Dir == DIR+"/male": return np.array(0)
    elif Dir == DIR+'/female' : return np.array(1)

IMG_SIZE = 300

def load_training_data():
    train_data = []
    TRAIN_DIR = [DIR+"/male", DIR+"/female"]
    for Dir in TRAIN_DIR:
        for img in os.listdir(Dir):
            label = label_img(Dir)
            path = os.path.join(Dir, img)
            if "DS_Store" not in path:               
                img = image.load_img(path, target_size=(224, 224))
                img_data = image.img_to_array(img)
                img_data = np.expand_dims(img_data, axis=0)
                img_data = preprocess_input(img_data)
                vgg16_feature = model.predict(img_data)
                vgg16_feature_np = np.array(vgg16_feature)
                train_data.append([vgg16_feature_np.flatten(), label])
    shuffle(train_data)
    return train_data

train_data = load_training_data()
trainImages = np.array([i[0] for i in train_data])
trainLabels = np.array([i[1] for i in train_data])

clf = svm.SVC(gamma='scale')
clf.fit(trainImages, trainLabels)
joblib.dump(clf, 'model_cnn_svm_vgg16.sav')


path = "/dataset/Students/thua/male_female/test/female/217702_1956-01-21_2013.jpg"
img = image.load_img(path, target_size=(224, 224))
img_data = image.img_to_array(img)
img_data = np.expand_dims(img_data, axis=0)
img_data = preprocess_input(img_data)
vgg16_feature = model.predict(img_data)
vgg16_feature_np = np.array(vgg16_feature)
test_data = [vgg16_feature_np.flatten()]
pred = clf.predict(test_data)
print(pred)


#img_path = '/dataset/Students/thua/male_female/test/male/44903_1921-03-11_1971.jpg'
#img = image.load_img(img_path, target_size=(224, 224))
#img_data = image.img_to_array(img)
#img_data = np.expand_dims(img_data, axis=0)
#img_data = preprocess_input(img_data)

#vgg16_feature = model.predict(img_data)

#print(vgg16_feature.shape)
