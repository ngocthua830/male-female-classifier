from PIL import Image
import numpy as np
import os
from random import shuffle
import matplotlib.pyplot as plt

#DIR = "/dataset/Students/thua/male_female/test"

#def label_img(Dir):
#    if Dir == DIR+"/male": return np.array([1, 0])
#    elif Dir == DIR+'/female' : return np.array([0, 1])

IMG_SIZE = 300

#def load_test_data():
#    test_data = []
#    TRAIN_DIR = [DIR+"/male", DIR+"/female"]
#    for Dir in TRAIN_DIR:
#        for img in os.listdir(Dir):
#            label = label_img(Dir)
#            path = os.path.join(Dir, img)
#            if "DS_Store" not in path:
#                img = Image.open(path)
#                img = img.convert('L')
#                img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
#                test_data.append([np.array(img), label])
#    shuffle(test_data)
#    return test_data

#test_data = load_test_data()
#testImages = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
#testLabels = np.array([i[1] for i in test_data])

import numpy as np
from keras.models import load_model
from keras.preprocessing import image
import sys
model = load_model('mf_model_1616-250119.h5')

#loss, acc = model.evaluate(testImages, testLabels, verbose = 0)
#print(acc * 100)

def predict_img(path):
    img = Image.open(path)
    img = img.convert('L')
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
    img_tensor = np.array(img)
    img_tensor = img_tensor.reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    pred = model.predict(img_tensor)
    return pred
if len(sys.argv) > 1:
    pred = predict_img(sys.argv[1])
else:
    pred = predict_img("/dataset/Students/thua/male_female/test/male/44903_1921-03-11_1971.jpg")

print('Ratio of male and female, respectively: ', pred)

if pred[0][0] > pred[0][1]:
   print('male')
else:
   print('female')

